// Constants
const ARIADNE_ID_PROOF_NS = 'http://ariadne.id/protocol/proof';

const DATA_NAMES = {
	'jabber:iq:roster': 'Contact list',
	'vcard-temp': 'Profile information',
};

const STATIC_SERVICE_URLS = {
	'blabber.im': 'https://blabber.im/http-bind',
	'libre-chat.net': 'https://libre-chat.net:5280/bosh',
};

const NS = {
    xrd: 'http://docs.oasis-open.org/ns/xri/xrd-1.0',
    roster: 'jabber:iq:roster',
    disco_items: 'http://jabber.org/protocol/disco#items',
    disco_info: 'http://jabber.org/protocol/disco#info',
    dataforms: 'jabber:x:data',
    pubsub: 'http://jabber.org/protocol/pubsub',
    pubsub_owner: 'http://jabber.org/protocol/pubsub#owner',
    avatar_metadata: 'urn:xmpp:avatar:metadata',
    avatar_data: 'urn:xmpp:avatar:data',
    nickname: 'http://jabber.org/protocol/nick',
    vcard4: 'urn:ietf:params:xml:ns:vcard-4.0',
    mam: 'urn:xmpp:mam:2',
    forward: 'urn:xmpp:forward:0',
};

// General functions
let nanoid=(t=21)=>crypto.getRandomValues(new Uint8Array(t)).reduce(((t,e)=>t+=(e&=63)<36?e.toString(36):e<62?(e-26).toString(36).toUpperCase():e>62?"-":"_"),"");

// IQ call functions
function queryNode(_converse, env) {
	console.log('Querying node...');
	return new Promise((resolve, reject) => {
		const request = env.$iq({
			type: 'get'
		})
		.c('query', {
			xmlns: 'http://jabber.org/protocol/disco#info',
			node: ARIADNE_ID_PROOF_NS
		});
		_converse.connection.sendIQ(request,
			stanza => {
				console.log('Querying node done!', stanza.querySelector(`iq query[node="${ARIADNE_ID_PROOF_NS}"]`));
				resolve(stanza.querySelector(`iq query[node="${ARIADNE_ID_PROOF_NS}"]`));
			},
			err => {
				console.log('Querying node failed: ', err);
				reject(err.querySelector('error :first-child').nodeName);
			}
		);
	});
}

function createNode(_converse, env) {
	console.log('Creating node...');
	return new Promise((resolve, reject) => {
		const request = env.$iq({
			type: 'set'
		})
		.c('pubsub', {
			xmlns: 'http://jabber.org/protocol/pubsub'
		})
		.c('create', {
			node: ARIADNE_ID_PROOF_NS
		})
		.up()
		.c('configure')
		.c('x', {
			xmlns: 'jabber:x:data',
			type: 'submit'
		})
		.c('field', {
			var: 'FORM_TYPE',
			type: 'hidden'
		})
		.c('value', {}, 'http://jabber.org/protocol/pubsub#node_config')
		.up()
		.c('field', { var: 'pubsub#title' })
		.c('value', {}, 'Ariadne Identity Proofs')
		.up()
		.c('field', { var: 'pubsub#max_items' })
		.c('value', {}, 'max')
		.up()
		.c('field', { var: 'pubsub#persist_items' })
		.c('value', {}, 'true')
		.up()
		.c('field', { var: 'pubsub#access_model' })
		.c('value', {}, 'open');

		_converse.connection.sendIQ(request,
			_ => {
				console.log('Creating node done!');
				resolve();
			},
			err => {
				console.log('Creating node failed: ', err);
				reject(err.querySelector('error :first-child').nodeName);
			}
		);
	});
}

function deleteNode(_converse, env) {
	console.log('Deleting node...');
	return new Promise((resolve, reject) => {
		const request = env.$iq({
			type: 'set'
		})
		.c('pubsub', {
			xmlns: 'http://jabber.org/protocol/pubsub#owner'
		})
		.c('delete', {
			node: ARIADNE_ID_PROOF_NS
		});

		_converse.connection.sendIQ(request,
			_ => {
				console.log('Deleting node done!');
				resolve();
			},
			err => {
				console.log('Deleting node failed: ', err);
				reject(err.querySelector('error :first-child').nodeName);
			}
		);
	});
}

function requestNodeConfig(_converse, env) {
	console.log('Requesting node config...');
	return new Promise((resolve, reject) => {
		const request = env.$iq({
			type: 'get'
		})
		.c('pubsub', {
			xmlns: 'http://jabber.org/protocol/pubsub'
		})
		.c('configure', {
			node: ARIADNE_ID_PROOF_NS
		});
		_converse.connection.sendIQ(request,
			stanza => {
				console.log('Requesting node config done!', stanza.querySelector(`iq pubsub configure[node="${ARIADNE_ID_PROOF_NS}"]`));
				resolve(stanza.querySelector(`iq pubsub configure[node="${ARIADNE_ID_PROOF_NS}"]`));
			},
			err => {
				console.log('Requesting node config failed: ', err);
				reject(err.querySelector('error :first-child').nodeName);
			}
		);
	});
}

function fetchProofs(_converse, env) {
	console.log('Fetching Ariadne proofs...');
	return new Promise((resolve, reject) => {
		const request = env.$iq({
			type: 'get'
		})
		.c('pubsub', {
			xmlns: 'http://jabber.org/protocol/pubsub'
		})
		.c('items', {
			node: ARIADNE_ID_PROOF_NS
		});
		_converse.connection.sendIQ(request,
			stanza => {
				console.log('Fetching Ariadne proofs done!', stanza.querySelector(`iq pubsub items[node="${ARIADNE_ID_PROOF_NS}"]`));
				resolve(stanza.querySelector(`iq pubsub items[node="${ARIADNE_ID_PROOF_NS}"]`));
			},
			err => {
				console.log('Fetching Ariadne proofs failed: ', err);
				reject(err.querySelector('error :first-child').nodeName);
			}
		);
	});
}

function uploadProof(_converse, env, proof) {
	console.log(`Uploading Ariadne proof ${proof}...`);

	const newItemId = nanoid()
	return new Promise((resolve, reject) => {
		let request = env.$iq({
			type: 'set'
		})
		.c('pubsub', {
			xmlns: 'http://jabber.org/protocol/pubsub'
		})
		.c('publish', {
			node: ARIADNE_ID_PROOF_NS
		})
		.c('item', {
			id: newItemId
		})
		.c('value', {}, proof)
		.up()
		.up()
		.c('publish-options')
		.c('x', {
			xmlns: 'jabber:x:data',
			type: 'submit'
		})
		.c('field', {
			var: 'FORM_TYPE',
			type: 'hidden'
		})
		.c('value', {}, 'http://jabber.org/protocol/pubsub#publish-options')
		.up()
		.c('field', { var: 'pubsub#persist_items' })
		.c('value', {}, 'true')
		.up()
		.c('field', { var: 'pubsub#access_model' })
		.c('value', {}, 'open');

		_converse.connection.sendIQ(request,
			_ => {
				console.log(`Uploading Ariadne proof ${proof} done! Item id: ${newItemId}`);
				resolve();
			},
			err => {
				console.warn(`Uploading Ariadne proof ${proof} failed:`, err);
				reject(err);
			}
		);
	});
}

function deleteProof(_converse, env, itemId) {
	console.log(`Deleting Ariadne proof with item id ${itemId}...`);
	return new Promise((resolve, reject) => {
		const request = env.$iq({
			type: 'set'
		})
		.c('pubsub', {
			xmlns: 'http://jabber.org/protocol/pubsub'
		})
		.c('retract', {
			node: ARIADNE_ID_PROOF_NS
		})
		.c('item', {
			id: itemId
		})
		.c('value', {}, itemId);

		_converse.connection.sendIQ(request,
			_ => {
				console.log(`Deleting Ariadne proof with item id ${itemId} done!`);
				resolve();
			},
			err => {
				console.warn(`Deleting Ariadne proof with item id ${itemId} failed:`, err);
				reject(err);
			}
		);
	});
}

// Converse helper functions
function setServiceUrl(_converse, service_url) {
	if(service_url.startsWith('wss:')) {
		_converse.api.settings.set('websocket_url', service_url);
	} else {
		_converse.api.settings.set('bosh_service_url', service_url);
	}
	sessionStorage.setItem('ariadne-proof-service-url', service_url);
}

function nsResolver(prefix) {
    return NS[prefix] || null;
}

function parseXPath(elem, xpath, result) {
    if (result === undefined)
        result = XPathResult.FIRST_ORDERED_NODE_TYPE;
    const value = elem.getRootNode().evaluate(xpath, elem, nsResolver, result, null);
    if (result == XPathResult.FIRST_ORDERED_NODE_TYPE)
        return value.singleNodeValue;
    return value;
}

function getServiceURL(jid) {
	const [nodepart, domainpart] = jid.split('@', 2);
	if(STATIC_SERVICE_URLS.hasOwnProperty(domainpart)) {
		return Promise.resolve(STATIC_SERVICE_URLS[domainpart]);
	}

	let xrdPromise = new Promise((resolve, reject) => {
		const url = `https://${domainpart}/.well-known/host-meta`;
		const xhr = new XMLHttpRequest();
		xhr.onabort = reject;
		xhr.onerror = reject;
		xhr.overrideMimeType('text/xml');
		xhr.onload = function(evt) {
			if(evt.target.status != 200) {
				reject();
				return;
			}
			const xml = evt.target.responseXML;
			if(!xml) {
				reject();
				return;
			}
			const links = parseXPath(xml, './xrd:XRD/xrd:Link', XPathResult.ORDERED_NODE_ITERATOR_TYPE);
			let bosh_service = null;
			let ws_service = null;
			while (true) {
				const link = links.iterateNext();
				if (!link)
					break;
				const rel = link.getAttributeNS(null, 'rel');
				if (rel == 'urn:xmpp:alt-connections:xbosh')
					bosh_service = link.getAttributeNS(null, 'href');
				else if (rel == 'urn:xmpp:alt-connections:websocket')
					ws_service = link.getAttributeNS(null, 'href');
			}
			if(ws_service || bosh_service) {
				resolve(ws_service || bosh_service);
			} else {
				reject();
			}
		};
		xhr.open('GET', url);
		xhr.send();
	});
	let jrdPromise = new Promise((resolve, reject) => {
		const url = 'https://' + domainpart + '/.well-known/host-meta.json';
		const xhr = new XMLHttpRequest();
		xhr.onabort = reject;
		xhr.onerror = reject;
		xhr.overrideMimeType('application/json');
		xhr.onload = function(evt) {
			if(evt.target.status != 200) {
				reject();
				return;
			}
			let jrd = null;
			switch(evt.target.responseType) {
				case 'json':
					jrd = evt.target.response;
					break;
				case '': // "An empty responseType string is the same as "text", the default type."
				case 'text':
					jrd = JSON.parse(evt.target.response);
					break;
				default:
					return reject();
			}

			let bosh_service = null;
			let ws_service = null;

			if(jrd.links) {
				for(link of jrd.links) {
					if (link.rel == 'urn:xmpp:alt-connections:xbosh')
						bosh_service = link.href;
					else if (link.rel == 'urn:xmpp:alt-connections:websocket')
						ws_service = link.href;
				}
			}
			if(ws_service || bosh_service) {
				resolve(ws_service || bosh_service);
			} else {
				reject();
			}
		};
		xhr.open('GET', url);
		xhr.send();
	});
	return Promise.any([xrdPromise, jrdPromise]).catch(err => {
		// If we wanted a fallback, here is a good place to return it
		throw "XEP-0156 lookup failed.";
	});
}

// Converse code
window.addEventListener('converse-loaded', e => {
	const { converse } = e.detail;
	const { Strophe } = converse.env;

	console.log('Converse loaded');

	converse.plugins.add('ariadne-proof', {
		initialize: function() {
			const { _converse } = this;
			const error_el = document.querySelector('#error-alert');
			const output_proofs_el = document.querySelector('.output--proofs');
			const output_conn_el = document.querySelector('.output--connection');

			const logged_in_panel = document.querySelector('#logged-in');
			const logging_in_panel = document.querySelector('#logging-in');
			const logged_out_panel = document.querySelector('#logged-out');
			
            const login_form = document.querySelector('form.login');
			const logout_form = document.querySelector('form.logout');
			const add_proof_form = document.querySelector('form.add-proof');
			const reset_node_form = document.querySelector('form.reset-node');
			const remove_node_form = document.querySelector('form.remove-node');

			function setUiState(state) {
				console.log(`Setting UI to state: ${state}`);
                
				logged_out_panel.style.display = 'none';
				logging_in_panel.style.display = 'none';
				logged_in_panel.style.display = 'none';

                switch (state) {
                    default:
                    case 'logged-out':
                        logged_out_panel.style.display = 'block';
                        break;
                    case 'logging-in':
                        logging_in_panel.style.display = 'block';
                        break;
                    case 'logged-in':
                        logged_in_panel.style.display = 'block';
                        break;
                }
			}

			function updateUiConn() {
				output_conn_el.innerText = `You are logged in as ${Strophe.getBareJidFromJid(_converse.connection.jid)}.`;
			}

			function updateUiProofs() {
				fetchProofs(_converse, converse.env)
				.catch(err => {
					if (err === 'item-not-found') {
						return null;
					} else {
						_converse.api.user.logout();
						return null;
					}
				})
				.then(results => {
					let output = '';

					if (!results) {
						output = 'The Ariadne node was not found. Press the <strong>Reset node</strong> button below to create it.';
					} else {
						if (results.querySelectorAll('item').length === 0) {
							output = 'No Ariadne proofs found. Add one below.';
						}
						for (item of results.querySelectorAll('item')) {
							output += `<p>${item.querySelector('value').textContent}
								<button data-action="delete-proof" data-proof-id="${item.getAttribute('id')}">Delete proof</button></p>`;
						}
					}


					output_proofs_el.innerHTML = output;

					document.querySelectorAll('button[data-action="delete-proof"]').forEach(el => {
						el.addEventListener('click', e => {
							deleteProof(_converse, converse.env, el.getAttribute('data-proof-id'))
							.then(_ => {
								updateUiProofs();
							})
						})
					})
				})
			}

			function setErrorMessage(message) {
				console.log(`Showing error message: ${message ? message : ''}`);
                error_el.innerText = message ? message : '';
			}

			login_form.addEventListener('submit', e => {
				e.preventDefault();

				const form_data = new FormData(e.target);
                setUiState('logging-in');
				setErrorMessage();

				getServiceURL(form_data.get('jid')).then(
					service_url => {
						console.log(`Discovered service URL: ${service_url}`);
						setServiceUrl(_converse, service_url);
						setTimeout(
							() => _converse.api.user.login(form_data.get('jid'), form_data.get('password')),
							0
						);
					},
					err => {
                        setUiState('logged-out');
						setErrorMessage(`Your account is not currently compatible with this service (${err}).`);
					}
				);
			});

			add_proof_form.addEventListener('submit', e => {
				e.preventDefault();

				const form_data = new FormData(e.target);
				uploadProof(_converse, converse.env, form_data.get('new_proof'))
				.then(
					_ => {
						console.log('Proof uploaded!');
						updateUiProofs();
					},
					err => console.error('Proof upload error: ', err)
				)
			});

			reset_node_form.addEventListener('submit', async e => {
				e.preventDefault();

				await queryNode(_converse, converse.env)
				.then(result => {
					result = result.querySelector('identity[type="pep"]');
					if (result && result.getAttribute('category') === 'pubsub') {
						deleteNode(_converse, converse.env)
					}
				})
                .then(_ => {
					createNode(_converse, converse.env);
                })
                .then(_ => {
					updateUiProofs();
                })
			});

			remove_node_form.addEventListener('submit', async e => {
				e.preventDefault();

				await deleteNode(_converse, converse.env)
				updateUiProofs();
			});

			logout_form.addEventListener('submit', e => {
				e.preventDefault();
				
                setUiState('logged-out');
				setErrorMessage();
				_converse.api.user.logout();
			});

			_converse.api.listen.on('connected', async () => {
				console.log('Converse connected to service');

				setUiState('logged-in');
                updateUiConn();
                
				await queryNode(_converse, converse.env)
				.then(result => {
					result = result.querySelector('identity[type="pep"]');
					if (!result || result.getAttribute('category') !== 'pubsub') {
						createNode(_converse, converse.env);
					}
				})
                .then(_ => {
					updateUiProofs();
                })
            });

			_converse.api.listen.on('disconnected', () => {
				console.log('Converse disconnected');
                
				let disconnect_feedback = _converse.connfeedback.get('message');
				if (disconnect_feedback) {
					setUiState('logged-out');
					setErrorMessage(disconnect_feedback);
				}
			});
		}
	});

	converse.initialize({
		// Provide a dummy BOSH URL (it's not expected to work, but
		// Converse.js won't initialize without one. We later discover
		// it dynamically and update before we call login().
		bosh_service_url: sessionStorage.getItem('ariadne-proof-service-url') || "https://localhost:5281/http-bind",

		authentication: 'login',
		auto_reconnect: false,
		persistent_store: 'sessionStorage',
		discover_connection_methods: false,
		whitelisted_plugins: ['ariadne-proof'],
		blacklisted_plugins: [
			'converse-vcard',
			'converse-muc',
			'converse-bookmarks',
			'converse-carbons',
		],
		priority: -1,
		debug: true
	});

});
