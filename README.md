# XAPU - XMPP Ariadne Proof Utility

This utility allows you to upload and manage Ariadne proofs in your XMPP account. 

Hosted at [xmpp-util.keyoxide.org](xmpp-util.keyoxide.org).

It's a static page that runs entirely in the browser so feel free to host it yourself on your own server.

## How it works

This utility logs in to your XMPP account and creates a new public pubsub node (if it doesn't exist yet) dedicated to Ariadne Identity proofs. You are able to create new proofs and remove existing proofs. There's a button to reset the pubsub node for troubleshooting and even the possibility to remove the pubsub node if you no longer wish to use proofs.

## Dependencies

The included files are:

- converse.js (headless) version 7.0.6
- simple.css version 2.1.1

## Credits

This utility is a modified version of the XMPP account exporter utility on [github](https://github.com/snikket-im/xmpp-account-exporter) by Matthew Wild.